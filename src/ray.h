//
// ray.h
//
// The low-level classes used by ray tracing: the ray and isect classes.
//

#ifndef __RAY_H__
#define __RAY_H__

// who the hell cares if my identifiers are longer than 255 characters:
#pragma once

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <memory>

class SceneObject;
class isect;

/*
 * ray_thread_id: a thread local variable for statistical purpose.
 */
extern thread_local unsigned int ray_thread_id;

// A ray has a position where the ray starts, and a direction (which should
// always be normalized!)

class ray {
public:
	enum RayType { VISIBILITY, REFLECTION, REFRACTION, SHADOW };

	ray(const glm::vec3& pp, const glm::vec3& dd, const glm::vec3& w,
	    RayType tt = VISIBILITY);
	ray(const ray& other);
	~ray();

	ray& operator=(const ray& other);

	glm::vec3 at(float t) const { return p + (t * d); }
	glm::vec3 at(const isect& i) const;

	glm::vec3 getPosition() const { return p; }
	glm::vec3 getDirection() const { return d; }
	glm::vec3 getAtten() const { return atten; }
	float getTime() const { return t; }

	void setPosition(const glm::vec3& pp) { p = pp; }
	void setDirection(const glm::vec3& dd) { d = dd; }
	void setTime(const float& tt) { t = tt; }

private:
	glm::vec3 p;
	glm::vec3 d;
	glm::vec3 atten;
	float t;
	
};


// The description of an intersection point.

class isect {
public:
	isect() : t(0.0){}
	isect(const isect& other)
	{
		copyFromOther(other);
	}

	~isect() { }

	isect& operator=(const isect& other)
	{
		copyFromOther(other);
		return *this;
	}

	// Get/Set Time of flight
	void setT(float tt) { t = tt; }
	double getT() const { return t; }


private:
	void copyFromOther(const isect& other)
	{
		if (this == &other)
			return ;
		t             = other.t;
	}

	float t;

};

const double RAY_EPSILON = 0.00001;

#endif // __RAY_H__
