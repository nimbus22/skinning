#ifndef BONE_GEOMETRY_H
#define BONE_GEOMETRY_H

#include <ostream>
#include <vector>
#include <string>
#include <map>
#include <limits>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <mmdadapter.h>
#include <iostream>
#include "ray.h"
#include "config.h"

class TextureToRender;
class Mesh;
class Configuration;

struct BoundingBox {
	BoundingBox()
		: min(glm::vec3(-std::numeric_limits<float>::max())),
		max(glm::vec3(std::numeric_limits<float>::max())) {}
	glm::vec3 min;
	glm::vec3 max;
	bool intersect(const ray& r, float& tMin, float& tMax) const;
};

struct Joint {
	Joint()
		: joint_index(-1),
		  parent_index(-1),
		  position(glm::vec3(0.0f)),
		  init_position(glm::vec3(0.0f)),
		  t_i(localTransform()),
		  d_i(glm::mat4(1.0f)),
		  b_ji(glm::mat4(1.0f)),
		  u_i(glm::mat4(1.0f)),
		  rel_orientation(glm::fquat(1, 0, 0, 0)),
		  orientation(glm::fquat(1, 0, 0, 0))
	{
		// u_i = glm::mat4(1.0f);
	}
	Joint(int id, glm::vec3 wcoord, int parent)
		: joint_index(id),
		parent_index(parent),
		position(wcoord),
		init_position(wcoord),
		init_rel_position(init_position),
		orientation(glm::fquat(1, 0, 0, 0)),
		rel_orientation(glm::fquat(1, 0, 0, 0)),
		t_i(localTransform()),
		d_i(glm::mat4(1.0f)),
		b_ji(glm::mat4(1.0f)),
		u_i(glm::mat4(1.0f))
	{
		// i
	}

	glm::mat4 rotateJoint(glm::fquat& r) {
		orientation = r * orientation;
		return localTransform();
	}

	glm::mat4 translateJoint(glm::vec3& t) {
		position += t;
		return localTransform();
	}

	glm::mat4 localTransform() {
		glm::mat4 rotation_matrix = glm::mat4_cast(rel_orientation);
		glm::mat4 translation_matrix = glm::mat4(1.0f);
		
		//translation_matrix = glm::translate(position - init_position);
		if (joint_index == -1) {
			glm::vec3 final_pos = position - init_position;
			translation_matrix[3][0] = final_pos.x;
			translation_matrix[3][1] = final_pos.y;
			translation_matrix[3][2] = final_pos.z;
		}
			// translation_matrix = glm::translate(translation_matrix, position - init_position);
		return translation_matrix * rotation_matrix;
	}
	// }

	glm::mat4 get_ui(Joint &pj) {
		b_ji = glm::mat4(1.0f);
		// glm::vec3 p_ji = glm::rotate(rel_orientation, init_rel_position);
		glm::vec3 p_ji = init_rel_position;
		b_ji[3][0] = p_ji.x;
		b_ji[3][1] = p_ji.y;
		b_ji[3][2] = p_ji.z;
		u_i = pj.u_i * b_ji * glm::mat4(1.0f);
		// glm::mat4 lt = localTransform();
		// std::cerr << "pj: " << p_ji.x << " " << p_ji.y << " " << p_ji.z << std::endl;
		return u_i;
	}

	glm::mat4 get_ui() {
		u_i = glm::mat4(1.0f);
		u_i[3][0] = init_position.x;
		u_i[3][1] = init_position.y;
		u_i[3][2] = init_position.z;
		// d_i = glm::translate(glm::mat4(1.0f), init_position) * t_i;
		u_i = u_i * glm::mat4(1.0f);
		return u_i;
	}
	glm::mat4 get_di(Joint& pj, std::vector<Joint>& joints) {
		b_ji = glm::mat4(1.0f);
		glm::vec3 p_ji = init_rel_position;
		// p_ji = rel_position;
		// std::cerr << joint_index << " " << p_ji.x << " " << p_ji.y << " " << p_ji.z << std::endl;
		// glm::vec3 p_ji = position - pj.position;
		// std::cerr << "get_di: " << joint_index << "141 " << p_ji.x << " " << p_ji.y << " " << p_ji.z << std::endl;
		b_ji[3][0] = p_ji.x;
		b_ji[3][1] = p_ji.y;
		b_ji[3][2] = p_ji.z;
		d_i = pj.d_i * b_ji * localTransform();
		// glm::mat4 lt = localTransform();
		// std::cerr << "pj: " << p_ji.x << " " << p_ji.y << " " << p_ji.z << std::endl;
		return d_i;
		
	}

	glm::mat4 get_di() {
		d_i = glm::mat4(1.0f);
		d_i[3][0] = init_position.x;
		d_i[3][1] = init_position.y;
		d_i[3][2] = init_position.z;
		// d_i = glm::translate(glm::mat4(1.0f), init_position) * t_i;
		d_i = d_i * localTransform();
		return d_i;
	}

	// glm::mat4 get_di(Joint& parent_joint) {
	// 	d_i = parent_joint.d_i * glm::translate(glm::mat4(1.0f), position - parent_joint.init_position) * localTransform();
	// }

	void set_matrices (Joint& parent_joint) {
		t_i = localTransform();
		b_ji = glm::translate(b_ji, position);
		glm::mat4 d_j = parent_joint.d_i;
		d_i = d_j * b_ji * t_i;
	}
	

	int joint_index;
	int parent_index;
	glm::vec3 position;             // position of the joint
	glm::fquat orientation;         // rotation w.r.t. initial configuration
	glm::fquat rel_orientation;     // rotation w.r.t. it's parent. Used for animation.
	glm::vec3 init_position;        // initial position of this joint
	glm::vec3 init_rel_position;    // initial relative position to its parent
	glm::vec3 rel_position;         // current relative position to its parent

	glm::mat4 t_i;                  // relative transformation matrix
	glm::mat4 u_i;                  // matrix that maps from local coordinates to world coordinates for undeformed model;
	glm::mat4 d_i;                  // matrix that maps from local coordinates to world coordinates for deformed model;
	glm::mat4 b_ji;                 // translation matrix from parent joint to this joint;
	glm::mat4 u_inverse;

	std::vector<int> children;
};

struct Configuration {
	std::vector<glm::vec3> trans;
	std::vector<glm::fquat> rot;

	const auto& transData() const { return trans; }
	const auto& rotData() const { return rot; }
};

struct KeyFrame {
	std::vector<glm::fquat> rel_rot;

	static void interpolate(const KeyFrame& from,
	                        const KeyFrame& to,
	                        float tau,
	                        KeyFrame& target);
};

struct LineMesh {
	std::vector<glm::vec4> vertices;
	std::vector<glm::uvec2> indices;
};

struct Cylinder {
	Cylinder();
	Cylinder(glm::vec3 p, glm::vec3 c, glm::fquat r) : p_(p), c_(c), v_(glm::normalize(c-p)), h_(glm::distance(p, c)) {
		r_ = glm::rotation(glm::vec3(0, 1, 0), v_);
		glm::mat4 rotation_matrix = mat4_cast(r_);
		v_ = glm::vec3(rotation_matrix[1][0], rotation_matrix[1][1], rotation_matrix[1][2]);
		n_ = glm::vec3(rotation_matrix[0][0], rotation_matrix[0][1], rotation_matrix[0][2]);
		b_ = glm::vec3(rotation_matrix[2][0], rotation_matrix[2][1], rotation_matrix[2][2]);
		// r_ = r;
	}

	void rollCylinder(glm::fquat& r) {
		glm::mat4 rotation_matrix = glm::mat4_cast(r);
		// r_ = r * r_;
		glm::mat4 matrix = glm::mat4(n_.x, n_.y, n_.z, 0,
									 v_.x, v_.y, v_.z, 0,
									 b_.x, b_.y, b_.z, 0,
									 0   , 0   , 0   , 1);
		matrix = rotation_matrix * matrix;
		n_ = glm::vec3(matrix[0][0], matrix[0][1], matrix[0][2]);
		v_ = glm::vec3(matrix[1][0], matrix[1][1], matrix[1][2]);
		b_ = glm::vec3(matrix[2][0], matrix[2][1], matrix[2][2]);
		c_ -= p_;
		glm::vec4 c4 = rotation_matrix * glm::vec4(c_, 1.0);
		c_ = glm::vec3(c4.x, c4.y, c4.z);
		c_ += p_;
		// translateCylinder(p_, c_, r);
	}
	
	void rotateCylinder(glm::fquat& r) {
		glm::mat4 rotation_matrix = glm::mat4_cast(r);
	    // r_ = r * r_;
		glm::mat4 matrix = glm::mat4(n_.x, n_.y, n_.z, 0,
									 v_.x, v_.y, v_.z, 0,
									 b_.x, b_.y, b_.z, 0,
									 0   , 0   , 0   , 1);
	    matrix = rotation_matrix * matrix;
	    n_ = glm::vec3(matrix[0][0], matrix[0][1], matrix[0][2]);
	    v_ = glm::vec3(matrix[1][0], matrix[1][1], matrix[1][2]);
		b_ = glm::vec3(matrix[2][0], matrix[2][1], matrix[2][2]);
		// glm::vec3 t_ = c_-p_;
		// t_ = glm::rotate(r, t_);
		// t_ += p_;
	    // c_ = t_;
	    // glm::vec4 c4 = matrix * glm::vec4(c_, 1.0);
		// c_ = glm::vec3(c4.x, c4.y, c4.z);
	    // // c_ += p_;
		// glm::mat4 rotation_matrix = glm::mat4_cast(r);
		// r_ = r * r_;
		// glm::mat4 matrix = glm::mat4(n_.x, n_.y, n_.z, 0,
		// 							 v_.x, v_.y, v_.z, 0,
		// 							 b_.x, b_.y, b_.z, 0,
		// 							 0   , 0   , 0   , 1);
		// matrix = rotation_matrix * matrix;
		// n_ = glm::vec3(matrix[0][0], matrix[0][1], matrix[0][2]);
		// v_ = glm::vec3(matrix[1][0], matrix[1][1], matrix[1][2]);
		// b_ = glm::vec3(matrix[2][0], matrix[2][1], matrix[2][2]);
		// translateCylinder(p_, c_, r);
	}
	
	void translateCylinder(glm::vec3& p, glm::vec3& c) {
		glm::quat rotate_to_vertical = glm::rotation(v_, glm::vec3(0, 1, 0));
		// v_ = glm::rotate(rotate_to_vertical, v_);
		n_ = glm::normalize(glm::rotate(rotate_to_vertical, n_));
		b_ = glm::normalize(glm::rotate(rotate_to_vertical, b_));
		// glm::mat4 matrix = glm::mat4(n_.x, n_.y, n_.z, 0,
		// 							 v_.x, v_.y, v_.z, 0,
		// 							 b_.x, b_.y, b_.z, 0,
		// 							 0   , 0   , 0   , 1);

		// glm::quat current = glm::toQuat(matrix);


	    p_ = p;
	    c_ = c;
	    v_ = glm::normalize(c-p);
	    h_ = glm::distance(p,c);

		glm::quat final = glm::rotation(glm::vec3(0, 1, 0), v_);
		n_ = glm::normalize(glm::rotate(final, n_));
		b_ = glm::normalize(glm::rotate(final, b_));
		// r_ = r * r_;
		// glm::mat4 rotation_matrix = mat4_cast(r_);
		// v_ = glm::vec3(rotation_matrix[1][0], rotation_matrix[1][1], rotation_matrix[1][2]);
		// n_ = glm::vec3(rotation_matrix[0][0], rotation_matrix[0][1], rotation_matrix[0][2]);
		// b_ = glm::vec3(rotation_matrix[2][0], rotation_matrix[2][1], rotation_matrix[2][2]);
	}
	bool intersect(const ray& r) const;
	glm::mat4 bone_transform() const;
	glm::vec3 p_;
	glm::vec3 c_;
	glm::vec3 v_;
	glm::vec3 n_;
	glm::vec3 b_;
	glm::fquat r_;
	float h_;

};

struct Bone {
	Bone(Joint& p_joint, Joint& c_joint, int id) : p_joint_(p_joint), c_joint_(c_joint), p(p_joint.position), c(p_joint.position), v(glm::normalize(c-p)), cylinder(p_joint.position, c_joint.position, c_joint.orientation), bone_id(id) {
		}

	void moveBone(Joint p_joint, Joint c_joint) {
		p_joint_ = p_joint;
		c_joint_ = c_joint;
		p = p_joint.position;
		c = c_joint.position;
		v = glm::normalize(c-p);
		cylinder.translateCylinder(p, c);
	}
	int bone_id;
	Joint p_joint_;
	Joint c_joint_;
	glm::vec3 p;
	glm::vec3 c;
	glm::vec3 v;
	Cylinder cylinder;
	BoundingBox bounds;

	bool intersect(const ray& r) const;
};

struct Skeleton {
	std::vector<Joint> joints;
	std::vector<Bone> bones;
	Joint world_joint;

	Configuration cache;

	void refreshCache(Configuration* cache = nullptr);
	const glm::vec3* collectJointTrans() const;
	const glm::fquat* collectJointRot() const;

	glm::mat4 getRecursiveD_i(Joint& j) {
		if (j.joint_index == -1) {
			return j.get_di();
		}
		glm::mat4 result = glm::mat4(1.0f);
		glm::mat4 b_ji(1.0f);
		glm::vec3 p_ji = glm::rotate(j.rel_orientation, j.init_rel_position);
		b_ji[3][0] = p_ji.x;
		b_ji[3][1] = p_ji.y;
		b_ji[3][2] = p_ji.z;

		if (j.parent_index != -1) {
			result = getRecursiveD_i(joints.at(j.parent_index)) * b_ji * j.localTransform();
		} else {
			result = getRecursiveD_i(world_joint) * b_ji * j.localTransform();
		}
		
		return result;
	}

	glm::mat4 getRecursiveU_i(Joint& j) {
		if (j.joint_index == -1) {
			return j.get_ui();
		}
		glm::mat4 result = glm::mat4(1.0f);
		glm::mat4 b_ji(1.0f);
		glm::vec3 p_ji = j.init_rel_position;
		b_ji[3][0] = p_ji.x;
		b_ji[3][1] = p_ji.y;
		b_ji[3][2] = p_ji.z;

		if (j.parent_index != -1) {
			result = getRecursiveU_i(joints.at(j.parent_index)) * b_ji * glm::mat4(1.0f);
		} else {
			result = getRecursiveU_i(world_joint) * b_ji * glm::mat4(1.0f);
		}
		
		return result;
	}

	// FIXME: create skeleton and bone data structures
	// void setJoints();
	int intersect(const ray& r) const;
};

struct Mesh {
	Mesh();
	~Mesh();
	std::vector<glm::vec4> vertices;
	/*
	 * Static per-vertex attrributes for Shaders
	 */
	std::vector<int32_t> joint0;
	std::vector<int32_t> joint1;
	std::vector<float> weight_for_joint0; // weight_for_joint1 can be calculated
	std::vector<glm::vec3> vector_from_joint0;
	std::vector<glm::vec3> vector_from_joint1;
	std::vector<glm::vec4> vertex_normals;
	std::vector<glm::vec4> face_normals;
	std::vector<glm::vec2> uv_coordinates;
	std::vector<glm::uvec3> faces;

	std::vector<Material> materials;
	BoundingBox bounds;
	BoundingBox bone_bounds;
	Skeleton skeleton;

	void loadPmd(const std::string& fn);
	int getNumberOfBones() const;
	glm::vec3 getCenter() const { return 0.5f * glm::vec3(bounds.min + bounds.max); }
	const Configuration* getCurrentQ() const; // Configuration is abbreviated as Q
	Configuration* getCurrentQ_();
	void updateAnimation(float t = -1.0);

	std::vector<glm::mat4> getCurrentT();
	std::vector<glm::mat4> getCurrentU();

	void saveAnimationTo(const std::string& fn);
	void loadAnimationFrom(const std::string& fn);

	int intersect(const ray& r) const;

	void transform_vertex_weights();
	void setRots();

private:
	void computeBounds();
	void computeNormals();
	Configuration currentQ_;
};


#endif
