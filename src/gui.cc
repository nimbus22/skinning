#include "gui.h"
#include "config.h"
#include <jpegio.h>
#include "bone_geometry.h"
#include <iostream>
#include <algorithm>
#include <debuggl.h>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include "ray.h"

namespace {
	// FIXME: Implement a function that performs proper
	//        ray-cylinder intersection detection
	// TIPS: The implement is provided by the ray-tracer starter code.
	// bool traceRay(ray& r, cons)
}



GUI::GUI(GLFWwindow* window, int view_width, int view_height, int preview_height)
	:window_(window), preview_height_(preview_height)
{
	glfwSetWindowUserPointer(window_, this);
	glfwSetKeyCallback(window_, KeyCallback);
	glfwSetCursorPosCallback(window_, MousePosCallback);
	glfwSetMouseButtonCallback(window_, MouseButtonCallback);
	glfwSetScrollCallback(window_, MouseScrollCallback);

	glfwGetWindowSize(window_, &window_width_, &window_height_);
	if (view_width < 0 || view_height < 0) {
		view_width_ = window_width_;
		view_height_ = window_height_;
	} else {
		view_width_ = view_width;
		view_height_ = view_height;
	}
	float aspect_ = static_cast<float>(view_width_) / view_height_;
	projection_matrix_ = glm::perspective((float)(kFov * (M_PI / 180.0f)), aspect_, kNear, kFar);
}

GUI::~GUI()
{
}

void GUI::assignMesh(Mesh* mesh)
{
	mesh_ = mesh;
	center_ = mesh_->getCenter();
}

void GUI::keyCallback(int key, int scancode, int action, int mods)
{
#if 0
	if (action != 2)
		std::cerr << "Key: " << key << " action: " << action << std::endl;
#endif
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window_, GL_TRUE);
		return ;
	}
	if (key == GLFW_KEY_J && action == GLFW_RELEASE) {
		//FIXME save out a screenshot using SaveJPEG
		std::string filename = "./aoeu.jpg";
		std::string file = "aoeu.jpg";
		
		unsigned char pixels[3*view_width_*view_height_];
		CHECK_GL_ERROR(glReadPixels(0, 0, view_width_, view_height_, GL_RGB, GL_UNSIGNED_BYTE, pixels));
		// for (int i = 0; i < 3*view_width_*view_height_; i++) {
		// 	std::cerr << int(pixels[i]) << " ";
		// }
		bool result = SaveJPEG(filename, view_width_, view_height_, pixels);
		if (result) {
			std::cout << "Encoding and saving to file '" << file << "'" << std::endl;
		}
		else {
			std::cout << "fail" << std::endl;
		}
	}
	if (key == GLFW_KEY_S && (mods & GLFW_MOD_CONTROL)) {
		if (action == GLFW_RELEASE)
			mesh_->saveAnimationTo("animation.json");
		return ;
	}

	if (mods == 0 && captureWASDUPDOWN(key, action))
		return ;
	if (key == GLFW_KEY_LEFT || key == GLFW_KEY_RIGHT) {
		if (current_bone_ != -1) {
			float roll_speed;
			if (key == GLFW_KEY_RIGHT)
				roll_speed = -roll_speed_;
			else
				roll_speed = roll_speed_;
			// FIXME: actually roll the bone here
			
			// glm::fquat r = glm::fquat(glm::cos(roll_speed), 0, glm::sin(roll_speed), 0);
			int bone = getCurrentBone();
			Bone& b = mesh_->skeleton.bones.at(bone);
			glm::vec3 axis = b.cylinder.v_;
			glm::fquat r = glm::angleAxis(roll_speed, axis);
			auto& rot = mesh_->skeleton.joints.at(bone).rel_orientation;
			rot = r * rot;
			b.cylinder.rollCylinder(r);
			pose_changed_ = true;
			transformJoint(bone);
		}	
	} else if (key == GLFW_KEY_C && action != GLFW_RELEASE) {
		fps_mode_ = !fps_mode_;
	} else if (key == GLFW_KEY_LEFT_BRACKET && action == GLFW_RELEASE) {
		current_bone_--;
		current_bone_ += mesh_->getNumberOfBones();
		current_bone_ %= mesh_->getNumberOfBones();
		Bone b = mesh_->skeleton.bones.at(current_bone_);
		std::cerr << b.p.x << " " << b.p.y << " " << b.p.z << " " << b.c.x << " " << b.c.y << " " << b.c.z << std::endl;
	} else if (key == GLFW_KEY_RIGHT_BRACKET && action == GLFW_RELEASE) {
		current_bone_++;
		current_bone_ += mesh_->getNumberOfBones();
		current_bone_ %= mesh_->getNumberOfBones();
		Bone b = mesh_->skeleton.bones.at(current_bone_);
		std::cerr << b.p.x << " " << b.p.y << " " << b.p.z << " " << b.c.x << " " << b.c.y << " " << b.c.z << std::endl;
	} else if (key == GLFW_KEY_T && action != GLFW_RELEASE) {
		transparent_ = !transparent_;
	} else if (key == GLFW_KEY_KP_8) {
		glm::vec3 t = glm::vec3(0, 0, -move_speed_);
		rotate_joint(t);
	} else if (key == GLFW_KEY_KP_2) {
		glm::vec3 t = glm::vec3(0, 0, move_speed_);
		rotate_joint(t);
	} else if (key == GLFW_KEY_KP_6) {
		glm::vec3 t = glm::vec3(move_speed_, 0, 0);
		rotate_joint(t);
	} else if (key == GLFW_KEY_KP_4) {
		glm::vec3 t = glm::vec3(-move_speed_, 0, 0);
		rotate_joint(t);
	} else if (key == GLFW_KEY_SPACE) {
		glm::vec3 t = glm::vec3(0, move_speed_, 0);
		rotate_joint(t);
	} else if (key == GLFW_KEY_TAB) {
		glm::vec3 t = glm::vec3(0, -move_speed_, 0);
		rotate_joint(t);
	} 

	// // FIXME: implement other controls here.
	// else if (key == GLFW_KEY_KP_8 || key == GLFW_KEY_KP_2 || key == GLFW_KEY_KP_4 || key == GLFW_KEY_KP_6 || key == GLFW_KEY_SPACE || key == GLFW_KEY_TAB) {
	// 	glm::vec3& root = mesh_->skeleton.joints.at(0).position;
	// 	glm::vec3 t;
	// 	std::cerr << root.x << " " << root.y << " " << root.z << std::endl;
	// 	if (key == GLFW_KEY_KP_2 && action == GLFW_RELEASE) t = glm::vec3(0, 0, move_speed_);
	// 	else if (key == GLFW_KEY_KP_6 && action == GLFW_RELEASE) t = glm::vec3(move_speed_, 0, 0);
	// 	else if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE) t = glm::vec3(0, move_speed_, 0);
	// 	else if (key == GLFW_KEY_KP_8 && action == GLFW_RELEASE) t = glm::vec3(0, 0, -move_speed_);
	// 	else if (key == GLFW_KEY_KP_4 && action == GLFW_RELEASE) t = glm::vec3(-move_speed_, 0, 0);
	// 	else if (key == GLFW_KEY_TAB && action == GLFW_RELEASE) t = glm::vec3(0, -move_speed_, 0);
		
	// }
}

void GUI::rotate_joint(glm::vec3& t) {
	// std::cerr << "rotate" << std::endl;
	glm::vec3& root = mesh_->skeleton.joints.at(0).position;
	glm::vec3& world = mesh_->skeleton.world_joint.position;
	glm::fquat rotation = glm::rotation(glm::vec3(0, 0, -1), -mesh_->skeleton.bones.at(0).cylinder.b_);
	if (rotation.w < 1.0 - RAY_EPSILON) 
	t = glm::rotate(rotation, t); // rotated movement direction
	// std::cerr << "t: " << t.x << " " << t.y << " " << t.z << std::endl;
	// move world position by t
	mesh_->skeleton.world_joint.position += t;
	glm::vec3& world_pos = mesh_->skeleton.world_joint.position;
	glm::vec4 w4 = mesh_->skeleton.world_joint.get_di() * glm::vec4(mesh_->skeleton.world_joint.init_position, 1.0);
	mesh_->skeleton.world_joint.position = glm::vec3(w4.x, w4.y, w4.z);
	pose_changed_ = true;
	transformJoint(0);
	// glm::mat4 d_p = parent.get_di();
	// glm::vec4 c = d_p * glm::vec4(child.init_rel_position, 1.0f);
	// child.position = glm::vec3(c.x, c.y, c.z);
	// std::cerr << root.x << " " << root.y << " " << root.z << std::endl;
	// std::cerr << world.x << " " << world.y << " " << world.z << std::endl;
}

// glm::mat4 GUI::mapping_matrix(int i, bool deformed) {
// 	Joint& j = mesh_->skeleton.world_joint;
// 	glm::mat4 t_i = glm::mat4(1.0f);
// 	if (i != -1) j = mesh_->skeleton.joints.at(i);
// 	if (deformed) t_i = j.localTransform();
// 	if (i == -1) {
// 		return glm::translate(glm::mat4(1.0f), j.init_position) * t_i;
// 	} else {
// 		Joint& parent_joint = mesh_->skeleton.world_joint;
// 		if (i != 0) parent_joint = mesh_->skeleton.joints.at(j.parent_index);
// 		glm::mat4 b_ji = glm::translate(glm::mat4(1.0f), j.position - parent_joint.init_position);
// 		return mapping_matrix(j.parent_index, deformed) *  b_ji * t_i;
// 	}
// }

void GUI::transformJoint(int i) {
	// auto q = mesh_->getCurrentQ_();
	Joint& j = mesh_->skeleton.joints.at(i);
	Bone& b = mesh_->skeleton.bones.at(i);
	// glm::vec3& c = b.c;
	// glm::vec3& p = b.p;
	// std::cerr << i << " " << p.x << " " << p.y << " " << p.z << " " << c.x << " " << c.y << " " << c.z << std::endl;
	glm::mat4 d_i;
	if (i != 0) {
		Joint& p_j = mesh_->skeleton.joints.at(j.parent_index);
		// if (p_j.joint_index != 0){
			// Joint& pp_j = mesh_->skeleton.joints.at(p_j.parent_index);
		d_i = mesh_->skeleton.getRecursiveD_i(j);
		glm::vec4 map = d_i * glm::vec4(mesh_->skeleton.world_joint.init_position, 1.0f);
		glm::vec3 before = glm::normalize(j.position - p_j.position);
		j.position = glm::vec3(map.x, map.y, map.z);
		glm::vec3 after = glm::normalize(j.position - p_j.position);
		// qrot = rot * qrot;
		// q->trans[j.joint_index] = j.position;
		// q->rot[j.joint_index] = j.rel_orientation;
		b.moveBone(p_j, j);
		
	} else {
		Joint& p_j = mesh_->skeleton.world_joint;
		d_i = j.get_di(p_j, mesh_->skeleton.joints);
		glm::vec4 map = d_i * glm::vec4(mesh_->skeleton.world_joint.init_position, 1.0f);
		glm::vec3 before = glm::normalize(j.position - p_j.position);
		j.position = glm::vec3(map.x, map.y, map.z);
		glm::vec3 after = glm::normalize(j.position - p_j.position);
		// qrot = rot * qrot;
		// q->trans[j.joint_index] = j.position;
		// q->rot[j.joint_index] = j.rel_orientation;
		b.moveBone(p_j, j);
	}
	// std::cerr << i << " " << p.x << " " << p.y << " " << p.z << " " << c.x << " " << c.y << " " << c.z << std::endl;
	
	
	// Joint& world = mesh_->skeleton.world_joint;
	for (int c : j.children) {
		transformJoint(c);
	}
}

bool GUI::castRay(double i, double j)
{
	bool result = false;
	double x = double(i)/double(view_width_);
	double y = double(j)/double(view_height_);

	double a = (view_width_ * 1.0f) / (view_height_ * 1.0f);
	//std::cerr << a << " " << view_width_ / view_height_ << std::endl ;
	double theta = double(kFov) * kPi/180;
	double h = 2 * double(kNear) * glm::tan(theta / 2);
	double l = 2 * a * double(kNear) * glm::tan(theta / 2);
	//std::cerr << view_width_ << " " << view_height_ << " " << view_width_ / view_height_ << " " << a << " " << h << " " << l << std::endl;
	x *= l;
	y *= h;
	x -= l / 2;
	y -= h / 2;
	// std::cerr << glm::sin(180) << " " << glm::sin(/2) << std::endl;
	//x -= 0.5;
	//y -= 0.5;
	glm::vec3 direction = glm::normalize(glm::vec3(x, y, kNear));
	glm::mat3 rotation = glm::mat3(tangent_.x, tangent_.y, tangent_.z,
		up_.x, up_.y, up_.z,
		look_.x, look_.y, look_.z);
	direction = glm::normalize(rotation * direction);
	//glm::vec3 direction = glm::normalize()
	r.setPosition(eye_);
	r.setDirection(direction);
	//std::cerr << direction.x << " " << direction.y << " " << direction.z << std::endl;
	direction = r.getDirection();
	glm::vec3 eye = r.getPosition();
	int bone = mesh_->intersect(r);
	result = setCurrentBone(bone);
	 //std::cerr << getCurrentBone() << std::endl;
	return result;
}

glm::uvec2 GUI::getPixel(glm::vec3& rotation_origin) {
	glm::vec3 direction = glm::normalize(rotation_origin - eye_);
	glm::fquat rotation = glm::rotation(look_, glm::vec3(0, 0, -1));
	direction = glm::rotate(rotation, direction);

	double a = (view_width_ * 1.0f) / (view_height_ * 1.0f);
	// std::cerr << a << " " << view_width_ / view_hight_ << std::endl ;
	// std::cerr << direction.x << " " << direction.y << " " << direction.z << std::endl;
	double theta = double(kFov) * kPi/180;
	double h = 2 * double(kNear) * glm::tan(theta / 2);
	double l = 2 * a * double(kNear) * glm::tan(theta / 2);
	double h2 = h / 2;
	double l2 = l / 2;
	double hypotenuse = glm::sqrt(kNear*kNear - h2*h2);
	double height = glm::sqrt(hypotenuse*hypotenuse - l2*l2);
	float m = glm::abs(float(height) / direction.z);
	// std::cerr << theta << " " << h << " " << l << " " << h2 << " " << l2 << " " << hypotenuse << " " << height << " " << m << std::endl;
	direction = m * direction;
	// std::cerr << direction.x << " " << direction.y << " " << direction.z << std::endl;
	direction.x += l2;
	direction.y += h2;
	// std::cerr << direction.x << " " << direction.y << " " << direction.z << std::endl;
	// std::cerr << direction.x * l / (view_width_ * 1.0f) << std::endl;;
	int x = (direction.x*1.0f) * (view_width_*1.0f)/(l*1.0f);
	int y = (direction.y*1.0f) * (view_height_*1.0f)/(h*1.0f);
	y = view_height_ - y;
	// std::cerr << x << " " << y << std::endl;
	return glm::uvec2(x, y);
}

void GUI::mousePosCallback(double mouse_x, double mouse_y)
{
	last_x_ = current_x_;
	last_y_ = current_y_;
	current_x_ = mouse_x;
	current_y_ = window_height_ - mouse_y;
	float delta_x = current_x_ - last_x_;
	float delta_y = current_y_ - last_y_;
	if (sqrt(delta_x * delta_x + delta_y * delta_y) < 1e-15)
		return;
	if (mouse_x > view_width_)
		return ;
	glm::vec3 mouse_direction = glm::normalize(glm::vec3(delta_x, delta_y, 0.0f));
	glm::vec2 mouse_start = glm::vec2(last_x_, last_y_);
	glm::vec2 mouse_end = glm::vec2(current_x_, current_y_);
	glm::uvec4 viewport = glm::uvec4(0, 0, view_width_, view_height_);

	bool drag_camera = drag_state_ && current_button_ == GLFW_MOUSE_BUTTON_RIGHT;
	bool drag_bone = drag_state_ && current_button_ == GLFW_MOUSE_BUTTON_LEFT;
	// std::cerr<<drag_state_<<" "<<drag_bone<<std::endl;
	if (drag_camera) {
		// std::cerr<<"gram"<<std::endl;
		glm::vec3 axis = glm::normalize(
				orientation_ *
				glm::vec3(mouse_direction.y, -mouse_direction.x, 0.0f)
				);
		orientation_ =
			glm::mat3(glm::rotate(rotation_speed_, axis) * glm::mat4(orientation_));
		tangent_ = glm::column(orientation_, 0);
		up_ = glm::column(orientation_, 1);
		look_ = glm::column(orientation_, 2);
	} else if (drag_bone && current_bone_ != -1) {
		// std::cerr << "dragon"<<std::endl;
		Bone& b = mesh_->skeleton.bones.at(current_bone_);
		glm::vec3 rotation_origin = b.p_joint_.position;
		glm::uvec2 screen_coordinates = getPixel(rotation_origin);
		glm::uvec2 mouse_coordinates = glm::uvec2(int(mouse_x), int(mouse_y));
		glm::vec2 vector = glm::normalize(glm::vec2(mouse_coordinates.x*1.0f - screen_coordinates.x*1.0f, mouse_coordinates.y*1.0f - screen_coordinates.y*1.0f));
		glm::uvec2 child_coordinates = getPixel(b.c_joint_.position);
		glm::vec2 vector2 = glm::normalize(glm::vec2(child_coordinates.x*1.0f - screen_coordinates.x*1.0f, child_coordinates.y*1.0f - screen_coordinates.x*1.0f));
		
		glm::vec2 mouse_movement = glm::normalize(glm::vec2((current_x_ - last_x_)*1.0f, (-current_y_ + last_y_)*1.0f));
		// glm::vec3 x = glm::cross(glm::vec3(vector2, 1.0f), glm::vec3(mouse_movement, 1.0f));
		float x = 1;
		float phi = glm::acos(glm::dot(vector2, mouse_movement));
		phi = glm::min(phi, glm::acos(glm::dot(-vector2, mouse_movement)));
		x = phi / kPi;
		// std::cerr << x << std::endl;
		// vector2 += screen_coordinates;
		// mouse_movement += screen_coordinates;
		float theta;
		glm::vec3 xcross = glm::cross(glm::vec3(vector2, 1.0f), glm::vec3(mouse_movement, 1.0f));
		// std::cerr << xcross.z << std::endl;
		if (xcross.z <= 0) theta = 1 /(kPi*25);
		else theta = -1/(kPi*25);

		// float theta;
		// theta = -x.z/(kPi*90);


		// float theta = glm::acos(glm::dot(vector, vector2));
		// theta /= 250;
		// glm::vec3 x = glm::cross(glm::vec3(vector2, 1.0f), glm::vec3(vector, 1.0f));
		// if (x.z > 0) theta = -x.z/(kPi*90);
		// else theta = x.z/(kPi*90);
		// std::cerr << theta << std::endl;
		glm::vec3 bc = b.c_joint_.position;
		glm::vec3 cc = b.cylinder.c_;
		// std::cerr << bc.x << " " << bc.y << " " << bc.z << " " << cc.x << " " << cc.y << " " << cc.z << std::endl;
		glm::fquat rotation = glm::angleAxis(theta, -look_);
		if (current_bone_ == 0) {
			auto& rot = mesh_->skeleton.world_joint.rel_orientation;
			rot = rotation*rot;	
			// rot = mesh_->skeleton.world_joint.orientation;
			// rot = rotation*rot;	
		}
		else {
			auto& rot = mesh_->skeleton.joints.at(mesh_->skeleton.joints.at(current_bone_).parent_index).rel_orientation;
			rot = rotation*rot;
			// rot = mesh_->skeleton.joints.at(mesh_->skeleton.joints.at(current_bone_).parent_index).orientation;
			// rot = rotation*rot;
		}
		// rot = mesh_->skeleton.joints.at(current_bone_).orientation;
		// rot = rotation*rot;
		// b.cylinder.rotateCylinder(rot);
		// Joint& p_joint = b.p_joint_;
		// Joint& c_joint = b.c_joint_;
		// glm::vec3 pc = c_joint.position - p_joint.position;
		// pc = rotate(rotation, pc);
		// pc += p_joint.position;
		// c_joint.position = pc;
		// b.moveBone(p_joint, c_joint);
		pose_changed_ = true;

		transformJoint(current_bone_);
		// std::cerr << screen_coordinates.x << " " <<screen_coordinates.y << std::endl;
		// FIXME: Handle bone rotation
		return ;
	}
	bool intersects = castRay(current_x_, current_y_);
}

void GUI::mouseButtonCallback(int button, int action, int mods)
{
	if (current_x_ <= view_width_) {
		drag_state_ = (action == GLFW_PRESS);
		current_button_ = button;
		return ;
	}
	// FIXME: Key Frame Selection
}

void GUI::mouseScrollCallback(double dx, double dy)
{
	if (current_x_ < view_width_)
		return;
	// FIXME: Mouse Scrolling
}

void GUI::updateMatrices()
{
	// Compute our view, and projection matrices.
	if (fps_mode_)
		center_ = eye_ + camera_distance_ * look_;
	else
		eye_ = center_ - camera_distance_ * look_;

	view_matrix_ = glm::lookAt(eye_, center_, up_);
	light_position_ = glm::vec4(eye_, 1.0f);

	aspect_ = static_cast<float>(view_width_) / view_height_;
	projection_matrix_ =
		glm::perspective((float)(kFov * (M_PI / 180.0f)), aspect_, kNear, kFar);
	model_matrix_ = glm::mat4(1.0f);
}

MatrixPointers GUI::getMatrixPointers() const
{
	MatrixPointers ret;
	ret.projection = &projection_matrix_;
	ret.model= &model_matrix_;
	ret.view = &view_matrix_;
	return ret;
}

bool GUI::setCurrentBone(int i)
{
	if (i < 0 || i >= mesh_->getNumberOfBones()){
		current_bone_ = i;
		return false;
	}
	current_bone_ = i;
	return true;
}

float GUI::getCurrentPlayTime() const
{
	return 0.0f;
}


bool GUI::captureWASDUPDOWN(int key, int action)
{
	if (key == GLFW_KEY_W) {
		if (fps_mode_)
			eye_ += zoom_speed_ * look_;
		else
			camera_distance_ -= zoom_speed_;
		return true;
	} else if (key == GLFW_KEY_S) {
		if (fps_mode_)
			eye_ -= zoom_speed_ * look_;
		else
			camera_distance_ += zoom_speed_;
		return true;
	} else if (key == GLFW_KEY_A) {
		if (fps_mode_)
			eye_ -= pan_speed_ * tangent_;
		else
			center_ -= pan_speed_ * tangent_;
		return true;
	} else if (key == GLFW_KEY_D) {
		if (fps_mode_)
			eye_ += pan_speed_ * tangent_;
		else
			center_ += pan_speed_ * tangent_;
		return true;
	} else if (key == GLFW_KEY_DOWN) {
		if (fps_mode_)
			eye_ -= pan_speed_ * up_;
		else
			center_ -= pan_speed_ * up_;
		return true;
	} else if (key == GLFW_KEY_UP) {
		if (fps_mode_)
			eye_ += pan_speed_ * up_;
		else
			center_ += pan_speed_ * up_;
		return true;
	}
	return false;
}


// Delegrate to the actual GUI object.
void GUI::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	GUI* gui = (GUI*)glfwGetWindowUserPointer(window);
	gui->keyCallback(key, scancode, action, mods);
}

void GUI::MousePosCallback(GLFWwindow* window, double mouse_x, double mouse_y)
{
	GUI* gui = (GUI*)glfwGetWindowUserPointer(window);
	gui->mousePosCallback(mouse_x, mouse_y);
}

void GUI::MouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	GUI* gui = (GUI*)glfwGetWindowUserPointer(window);
	gui->mouseButtonCallback(button, action, mods);
}

void GUI::MouseScrollCallback(GLFWwindow* window, double dx, double dy)
{
	GUI* gui = (GUI*)glfwGetWindowUserPointer(window);
	gui->mouseScrollCallback(dx, dy);
}
