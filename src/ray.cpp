#include "ray.h"

ray::ray(const glm::vec3& pp,
	 const glm::vec3& dd,
	 const glm::vec3& w,
         RayType tt)
        : p(pp), d(dd), atten(w), t(tt)
{
}

ray::ray(const ray& other) : p(other.p), d(other.d), atten(other.atten)
{
}

ray::~ray()
{
}

ray& ray::operator=(const ray& other)
{
	p     = other.p;
	d     = other.d;
	atten = other.atten;
	t     = other.t;
	return *this;
}

glm::vec3 ray::at(const isect& i) const
{
	return at(i.getT());
}

thread_local unsigned int ray_thread_id = 0;
