R"zzz(#version 330 core
uniform mat4 bone_transform; // transform the cylinder to the correct configuration
const float kPi = 3.1415926535897932384626433832795;
uniform mat4 projection;
uniform mat4 model;
uniform mat4 view;
in vec4 vertex_position;
// FIXME: Implement your vertex shader for cylinders
// Note: you need call sin/cos to transform the input mesh to a cylinder

void main() {
    mat4 mvp = projection * view * model;
    
    vec4 final_position = vec4(sin(vertex_position.x * 2 * kPi)/(1.25 * kPi), vertex_position.y, cos(vertex_position.x * 2 * kPi)/(1.25 * kPi), 1);
    gl_Position = mvp * bone_transform * (final_position);
}
)zzz"
