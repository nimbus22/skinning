R"zzz(
#version 330 core
uniform vec4 light_position;
uniform vec3 camera_position;

uniform vec3 joint_trans[128];
uniform vec4 joint_rot[128];
uniform mat4 tx_d[128];
uniform mat4 tx_u[128];

in int jid0;
in int jid1;
in float w0;
in vec3 vector_from_joint0;
in vec3 vector_from_joint1;
in vec4 normal;
in vec2 uv;
in vec4 vert;

out vec4 vs_light_direction;
out vec4 vs_normal;
out vec2 vs_uv;
out vec4 vs_camera_direction;

vec3 qtransform(vec4 q, vec3 v) {
	return v + 2.0 * cross(cross(v, q.xyz) - q.w*v, q.xyz);
}

// void Mesh::transform_vertex_weights() {
// 	for (int i = 0; i < vertices.size(); i++) {
// 		glm::vec4& vertex = vertices.at(i);
// 		Joint& j0 = skeleton.joints.at(joint0.at(i));
// 		Joint& j1 = skeleton.joints.at(joint1.at(i));
// 		glm::vec3 p0 = j0.position;
// 		glm::vec3 p1 = j1.position;
// 		glm::mat4 u0 = j0.u_i;
// 		glm::mat4 u1 = j1.u_i;
// 		glm::mat4 d0 = j0.get_di(skeleton.joints.at(j0.parent_index), skeleton.joints);
// 		glm::mat4 d1 = j1.get_di(skeleton.joints.at(j1.parent_index), skeleton.joints);
// 		glm::vec3 vj0 = vector_from_joint0.at(i);
// 		glm::vec3 vj1 = vector_from_joint1.at(i);
// 		float weight0 = weight_for_joint0.at(i);
// 		float weight1 = weight0 * glm::length(vj1) / glm::length(vj0); 
// 		glm::vec4 sum = glm::vec4(weight0 * d0 * glm::inverse(u0) * vertex) + glm::vec4(weight0 * d0 * glm::inverse(u0) * vertex);
// 	}
	
// }
// // input: unit quaternion 'q0', translation vector 't' 
// // output: unit dual quaternion 'dq'
// void Mesh::QuatTrans2UDQ(const float q0[4], const float t[3], 
//                   float dq[2][4])
// {
	// non-dual part (just copy q0):
//    for (int i=0; i<4; i++) dq[0][i] = q0[i];
//    // dual part:
//    dq[1][0] = -0.5*(t[0]*q0[1] + t[1]*q0[2] + t[2]*q0[3]);
//    dq[1][1] = 0.5*( t[0]*q0[0] + t[1]*q0[3] - t[2]*q0[2]);
//    dq[1][2] = 0.5*(-t[0]*q0[3] + t[1]*q0[0] + t[2]*q0[1]);
//    dq[1][3] = 0.5*( t[0]*q0[2] - t[1]*q0[1] + t[2]*q0[0]);
// }


void main() {
	// FIXME: Implement linear skinning here
	mat2x4 b;
	vec4 q0 = joint_rot[jid0];
	vec3 t = joint_trans[jid0];
	mat2x4 dq;
	for (int i = 0; i < 4; i++) dq[0][i] = q0[i];
	dq[1][0] = -0.5*(t[0]*q0[1] + t[1]*q0[2] + t[2]*q0[3]);
	dq[1][1] = 0.5*( t[0]*q0[0] + t[1]*q0[3] - t[2]*q0[2]);
	dq[1][2] = 0.5*(-t[0]*q0[3] + t[1]*q0[0] + t[2]*q0[1]);
	dq[1][3] = 0.5*( t[0]*q0[2] - t[1]*q0[1] + t[2]*q0[0]);
	dq = w0 * dq;
	b = dq;
	if (jid1 != -1) {
		vec4 q1 = joint_rot[jid1];
		vec3 t1 = joint_trans[jid1];
		mat2x4 dq1;
		for (int i = 0; i < 4; i++) dq1[0][i] = q1[i];
		dq1[1][0] = -0.5*(t1[0]*q1[1] + t1[1]*q1[2] + t1[2]*q1[3]);
		dq1[1][1] = 0.5*( t1[0]*q1[0] + t1[1]*q1[3] - t1[2]*q1[2]);
		dq1[1][2] = 0.5*(-t1[0]*q1[3] + t1[1]*q1[0] + t1[2]*q1[1]);
		dq1[1][3] = 0.5*( t1[0]*q1[2] - t1[1]*q1[1] + t1[2]*q1[0]);
		dq1 = (1-w0) * dq1;
		b += dq1;
	}
	vec4 c0 = normalize(vec4(dq[0][0], dq[0][1], dq[0][2], dq[0][3]));
	vec4 ce = normalize(vec4(dq[1][0], dq[1][1], dq[1][2], dq[1][3]));
	float w0 = c0[0];
	float x0 = c0[1];
	float y0 = c0[2];
	float z0 = c0[3];
	float we = ce[0];
	float xe = ce[1];
	float ye = ce[2];
	float ze = ce[3];
	float t0 = 2*(-we*x0 + xe*w0 - ye*z0 + ze*y0);
	float t1 = 2*(-we*y0 + xe*z0 + ye*w0 - ze*x0);
	float t2 = 2*(-we*z0 - xe*y0 + ye*x0 + ze*w0);

	mat4 M = mat4(1 - 2*y0*y0 - 2*z0*z0, 2*x0*y0 - 2*w0*z0, 2*x0*z0 + 2*w0*y0, t0,
				  2*x0*y0 + 2*w0*z0, 1 - 2*x0*x0 - 2*z0*z0, 2*y0*z0 - 2*w0*x0, t1,
				  2*x0*z0 - 2*w0*y0, 2*y0*z0 + 2*w0*x0, 1 - 2*x0*x0 - 2*y0*y0, t2,
				  0, 0, 0, 1);
	vec4 position = M * vert;


	// vec4 position;
	// vec4 quat_rot = 
	if (jid1 != -1) {
		// vec3 vj0 = vec3(vert.x, vert.y, vert.z) - joint_trans[jid0];
		// vec3 vj1 = vec3(vert.x, vert.y, vert.z) - joint_trans[jid0];
		// position = vec4(w0 * tx_d[jid0] * tx_u[jid0] * vert) + vec4((w0 * joint_trans[jid0]), 1.0) + vec4((1-w0) * tx_d[jid1] * tx_u[jid1] * vert) + vec4(((1-w0) * joint_trans[jid1]), 1.0f);
		position = vec4(w0 * tx_d[jid0] * (tx_u[jid0]) * vert) + vec4((1-w0) * tx_d[jid1] * (tx_u[jid1]) * vert);
	} else {
		vec4 r = tx_u[jid0] * vert;
		vec3 qrj = qtransform(joint_rot[jid0], vec3(r.x, r.y, r.z)) + joint_trans[jid0];
		// qrj += vec3(0,0,0);
		position = vec4(qrj, 0.0);
	}
	
	gl_Position = position;
	vs_normal = normal;
	vs_light_direction = light_position - gl_Position;
	vs_camera_direction = vec4(camera_position, 1.0) - gl_Position;
	vs_uv = uv;
}
)zzz"
